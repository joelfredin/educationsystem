#pragma once

#include<iostream>
#include<string>

class Person
{
protected:
    std::string firstName;
    std::string lastName;

public:
    Person(std::string firstName, std::string lastName)
    {
        this->firstName = firstName;
        this->lastName = lastName;
    }
    Person()
    {
        
    }
    std::string first_name()
    {
        return firstName;
    }
    std::string last_name()
    {
        return lastName;
    }

    template <class B>
    void serialize(B &buf) const
    {
        buf << firstName << lastName;
    }

    template <class B>
    void parse(B &buf)
    {
        buf >> firstName >> lastName;
    }

};

