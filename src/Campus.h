#pragma once

#include<iostream>
#include<string>
#include<vector>
#include "Student.h"

class Building
{
private:
    std::string name;
    std::string department;

public:
    Building(std::string name, std::string department)
    {
        this->name = name;
        this->department = department;
    }

};

class Campus
{
private:
    std::string name;
    std::vector<Building> departments;
    std::vector<Student> all_students;
public:
    Campus(std::string name)
    {
        this->name = name;
    }
    void add_students(Student student)
    {
        all_students.push_back(student);
    }
    void add_N_students(int N)
    {
        std::cout << "add something" << std::endl;
        for(int i = 0; i < N; i++)
        {
            std::string firstname;
            std::string lastname;
            std::cin >> firstname;
            std::cin >> lastname;
            Student student(firstname, lastname,i);

            add_students(student);
        }
    }
};

class School
{
private:
    std::string name;
    std::vector<Campus> campuses;
    std::vector<Student> all_students;
    
public:
    School(std::string name)
    {
        this->name = name;
    }
    std::string return_name()
    {
        return name;
    }
    void add_campus(Campus campus)
    {
        campuses.push_back(campus);
    }
    void add_student(Student student)
    {
        all_students.push_back(student);
    }
    void add_N_students(int N)
    {
        for(int i = 0; i < N; i++)
        {
            std::string firstname;
            std::string lastname;
            int id;
            std::cout << "Please, write the first name of the person" << std::endl;
            std::cin >> firstname;
            std::cout << std::endl << "Please, write the last name of the person" << std::endl;
            std::cin >> lastname;
            std::cout << std::endl << "Please, type the id of the person" << std::endl;
            std::cin >> id;
            Student student(firstname, lastname,id);

            add_student(student);
        }
    }
    void add_campuses(int N)
    {
        for(int i = 0; i < N; i++)
        {
            std::string campus_name;
            std::cout << "Please, write the first name of the campus" << std::endl;
            std::cin >> campus_name;
            Campus campus(campus_name);

            add_campus(campus);
        }
    }

};