#pragma once

#include<iostream>
#include<string>
#include<vector>

class Book
{
private:
    std::string title;
    std::string genre;

public:
    Book(std::string title, std::string genre)
    {
        this->title = title;
        this->genre = genre;
    }
    void my_title()
    {
        std::cout << title << std::endl;
    }
};

class Library
{
private:
    std::string name;
    std::vector<Book> vector_of_books;

public:
    Library(std::string name)
    {
        this->name = name;
        //this->vector_of_books = vector_of_books;
    }
    void order_books(Book book)
    {
        vector_of_books.push_back(book);
    }
    void print_elements()
    {
        vector_of_books[0].my_title();
    }
    void print_books()
    {
        for(Book some_book : vector_of_books)
        {
            some_book.my_title();
        }
    }
    void add_collection_of_books(int const& N) 
    {
        for(int i = 0; i < N; i++)
        {
            std::string title;
            std::string genre;
            std::getline(std::cin, title);
            std::getline(std::cin, genre);
            Book book(title, genre);
            order_books(book);
        }
    }

};