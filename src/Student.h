#pragma once

#include<iostream>
#include<tuple>
//#include "Library.h"
#include "Person.h"

class Student : public Person
{
private:
    int id;
    std::vector<std::pair<Book, Library>> loaned_books;

public:
    Student(std::string first_name, std::string last_name, int id)
    :Person(first_name, last_name){
      this->id = id;
  	}
    Student() : Person()
    {

    }

    int return_id()
    {
        return id;
    }
    
    void print_student()
    {
        std::cout << first_name() << " " << last_name();
    }
    
    template <class B>
    void serialize(B &buf) const
    {
        Person::serialize(buf);
        buf << id;
    }

    template <class B>
    void parse(B &buf)
    {
        Person::parse(buf);
        buf >> id;
    }
    /*
    void Person::print_student()
    {
        std::cout << firstName << std::endl;
    }
    */
    /*
    void loan_books()
    {
        Book book_to_loan;
        Library lib;
        for(int i = 0; i < 4; i++)
        {
            loaned_books.push_back(std::pair<book_to_loan, lib>);
        }
    }
    */
};