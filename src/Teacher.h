#pragma once
#include<vector>
#include "Person.h"

class Teacher : public Person {
  private:
  	std::string department;
    int id;
  public:
    Teacher() : Person()
    {

    }
  	Teacher(std::string firstName, std::string lastName, std::string department, int id)
    :Person(firstName, lastName){
      this->department = department;
      this->id = id;
  	}

    template <class B>
    void serialize(B &buf) const
    {
        Person::serialize(buf);
        buf << id << department;
    }

    template <class B>
    void parse(B &buf)
    {
        Person::parse(buf);
        buf >> id >> department;
    }

    /*
    std::string GetName(){
  		return firstName + " " + lastName;
  	}
    */
    
};