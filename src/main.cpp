//#include "Campus.h"
#include <cassert>
#include<iostream>
#include<string>
#include <iomanip>
#include<fstream>
//#include "Person.h"
#include "Library.h"
#include "./hps/src/hps.h"
#include "Student.h"
#include "Course.h"
#include "Teacher.h"
#include "Campus.h"

std::vector<Course> construct_courses(int N)
{
    std::string name;
    std::string field_of_study;
    std::vector<Course> course_list;
    for(int i = 0; i < N; i++)
    {
        std::cout << "Please, tell me what the name of the course number " + std::to_string(i) + "!" << std::endl;
        std::cin >> name;
        std::cout << std::endl << "Please, tell me what field the course will be in." << std::endl;
        std::cin >> field_of_study;
        Course course(name, field_of_study);
        course_list.push_back(course);

    }
    return course_list;
}


std::vector<Teacher> all_teachers;

void add_teachers(int N)
{
    std::string first_name;
    std::string last_name;
    std::string department;
    int id;

    for(int i = 0; i < N; i++)
    {
        std::cin >> first_name;
        std::cin >> last_name;
        std::cin >> department;
        std::cin >> id;
        Teacher teacher(first_name, last_name, department, id);
    
        all_teachers.push_back(teacher);
    }
}
/*
void add_students(int N)
{
    std::string first_name;
    std::string last_name;
    int id;

    for(int i = 0; i < N; i++)
    {
        std::cin >> first_name;
        std::cin >> last_name;
        std::cin >> id;
        Student student(first_name, last_name, id);

        all_students.push_back(student);
    }
}
*/

std::vector<Course> all_courses;
void add_courses(int N)
{
    std::string course_name;
    std::string field_of_study;

    for(int i = 0; i < N; i++)
    {
        std::cin >> course_name;
        std::cin >> field_of_study;
        Course course(course_name, field_of_study);
    
        all_courses.push_back(course);
    }
}

std::string find_id(int id, std::vector<Student> list_of_students)
{
    for(Student student : list_of_students)
    {
        if(id == student.return_id())
        {
            return student.first_name() + " " + student.last_name();
        }
    }
    return "Didn't find that student!";
}

void print_course_names(std::vector<Course> courses)
{
    int i = 0;
    for(Course course : courses)
    {
        std::cout << i << " " << course.return_name() << std::endl;
        i++;
    }
}

void print_student_names(std::vector<Student> students)
{
    int i = 0;
    for(Student student : students)
    {
        std::cout << i << " " << student.first_name() << " " << student.last_name() << std::endl;
        i++;
    }
}

void print_teacher_names(std::vector<Teacher> teachers)
{
    int i = 0;
    for(Teacher teacher : teachers)
    {
        std::cout << i << " " << teacher.first_name() << " " << teacher.last_name() << std::endl;
        i++;
    }
}

int main()
{
    

    // Initializing some variables to be used in order to make choices in the menu
    char input;
    int second_input;
    int third_input;
    std::string name_of_school;
    std::string course_name, course_field_of_study;
    std::string first_name, last_name, department;
    int id;
    // Vectors which stores courses, students and teachers in separate lists.
    std::vector<Course> all_courses;
    std::vector<Student> all_students;
    std::vector<Teacher> all_teachers;
    std::cout << "Welcome to the educational system, here I am sure you will have a lot of fun! You will here have the chance to control your own educational system!" << std::endl;
    std::cout << "Let us begin this journey by creating our very own school! What do you want the name of the school to be?" << std::endl << std::endl;
    getline(std::cin, name_of_school);
    School school(name_of_school);
    std::cout << std::endl << std::endl << "Alright, welcome the school of " + school.return_name() << std::endl;
    std::cout << "It would also be cool if we had some campuses, how many do you wish to add?" << std::endl << std::endl;
    std::cin >> second_input;
    std::cout << std::endl << std::endl << "Alright let us build " + std::to_string(second_input) << " different campuses!" << std::endl << std::endl;
    school.add_campuses(second_input);
    
    // Loop for the program
    while(true)
    {
        std::cout << "I will now present you with the possible choices. Press the letter to the left, to explore what's on the right:" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'C\'" << std::right << std::setfill('.') << std::setw(20) << " To handle courses" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'S\'" << std::right << std::setfill('.') << std::setw(20) << " To handle students" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'T\'" << std::right << std::setfill('.') << std::setw(20) << " To handle teachers" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'L\'" << std::right << std::setfill('.') << std::setw(20) << " To discover the library" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'Q\'" << std::right << std::setfill('.') << std::setw(20) << " To quit the program" << std::endl << std::endl;
        std::cin >> input;
        std::cout << std::endl << std::endl;
        if(input == 'C')
        {
            std::cout << "Ahh, I see that you are interested in courses. Would you like to construct a new course, or add students to courses?" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'1\'" << std::right << std::setfill('.') << std::setw(20) << "To construct a new course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'2\'" << std::right << std::setfill('.') << std::setw(20) << " To add students to a course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'3\'" << std::right << std::setfill('.') << std::setw(20) << " To add teachers to the course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'4\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to serialize the course information" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'5\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to display the information of the serialized file" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'6\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to remove a student from a course " << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'7\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to remove a teacher from a course" << std::endl;
            std::cin >> second_input;
            if(second_input == 1)
            {
                std::cout << "Please, tell me the name of the course" << std::endl;
                std::cin >> course_name;
                std::cout << std::endl << "Now, tell me which area the course belongs to:" << std::endl;
                std::cin >> course_field_of_study;
                Course course(course_name, course_field_of_study);
                all_courses.push_back(course);
            }
            if(second_input == 2)
            {
                int specify_course;
                int specify_student;
                std::cout << "Here are all the available courses one can choose from:" << std::endl;
                print_course_names(all_courses);
                std::cout << std::endl << "Please, tell me which course-number you would like to add students to: " << std::endl;
                std::cin >> specify_course;
                std::cout << std::endl << "Now, let us list all of the possible students to add:" << std::endl;
                print_student_names(all_students);
                std::cout << std::endl << "Please, tell me which index of the student you would like to the course: " << std::endl;
                std::cin >> specify_student;
                std::cout << std::endl << "Great! Let us now add that student to the course...";
                all_courses.at(specify_course).add_students(all_students.at(specify_student));
            }
            if(second_input == 3)
            {
                int specify_course;
                int specify_teacher;
                std::cout << "Here are all the available courses one can choose from:" << std::endl;
                print_course_names(all_courses);
                std::cout << std::endl << "Please, tell me which course-number you would like to add teachers to: " << std::endl;
                std::cin >> specify_course;
                std::cout << std::endl << "Now, let us list all of the possible teachers to add:" << std::endl;
                print_teacher_names(all_teachers);
                std::cout << std::endl << "Please, tell me which index of the student you would like to the course: " << std::endl;
                std::cin >> specify_teacher;
                std::cout << std::endl << "Great! Let us now add that teacher to the course...";
                all_courses.at(specify_course).add_teachers(all_teachers.at(specify_teacher));
            }
            if(second_input == 4)
            {
                std::string serialized = hps::to_string(all_courses);
                auto parsed = hps::from_string<std::vector<Course>>(serialized);

                std::ofstream dataFile;
                dataFile.open("course.txt", std::ios::binary | std::ios::app);
                dataFile << serialized << std::endl;
                dataFile.close();
            }
            if(second_input == 5)
            {
                std::string serialized = hps::to_string(all_courses);
                auto parsed = hps::from_string<std::vector<Course>>(serialized);
                for(int i = 0; i < all_courses.size(); i++)
                {
                    Course c2 = parsed.at(i);
                    for(int j = 0; j < all_courses.size(); j++)
                    {
                        std::cout << c2.return_name() << std::endl;
                        std::cout << c2.students_in_course.at(j).first_name() << std::endl;
                    }
                }
            }
            if(second_input == 6)
            {
                int specify_course;
                int specify_student;
                std::cout << "Here are all the available courses one can choose from:" << std::endl;
                print_course_names(all_courses);
                std::cout << std::endl << "Please, tell me which course-number you would like to remove a student from: " << std::endl;
                std::cin >> specify_course;
                std::cout << std::endl << "Now, let us list all of the possible students to remove:" << std::endl;
                print_student_names(all_students);
                std::cout << std::endl << "Please, tell me which index of the student you would like to remove from the course: " << std::endl;
                std::cin >> specify_student;
                std::cout << std::endl << "Great! Let us now remove that student to the course...";
                all_courses.at(specify_course).remove_students(specify_student);
            }
            if(second_input == 7)
            {
                int specify_course;
                int specify_teachers;
                std::cout << "Here are all the available courses one can choose from:" << std::endl;
                print_course_names(all_courses);
                std::cout << std::endl << "Please, tell me which course-number you would like to remove a teacher from: " << std::endl;
                std::cin >> specify_course;
                std::cout << std::endl << "Now, let us list all of the possible students to remove:" << std::endl;
                print_student_names(all_students);
                std::cout << std::endl << "Please, tell me which index of the teacher you would like to remove from the course: " << std::endl;
                std::cin >> specify_teachers;
                std::cout << std::endl << "Great! Let us now remove that teacher to the course...";
                all_courses.at(specify_course).remove_teachers(specify_teachers);
            }

        }
        
        if(input == 'S')
        {
            std::cout << "Ahh, I see that you are interested in students." << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'1\'" << std::right << std::setfill('.') << std::setw(20) << " To construct a new student" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'2\'" << std::right << std::setfill('.') << std::setw(20) << " To find a student based on their ID" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'3\'" << std::right << std::setfill('.') << std::setw(20) << " To borrow a book" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'4\'" << std::right << std::setfill('.') << std::setw(20) << " To hand back a book" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'5\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to serialize the student information" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'6\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to display the information of the serialized file" << std::endl;
            std::cin >> second_input;
            if(second_input == 1)
            {
                std::cout << "Please, tell me the first name of the student" << std::endl;
                std::cin >> first_name;
                std::cout << std::endl << "Now, tell me the last name of the student" << std::endl;
                std::cin >> last_name;
                std::cout << std::endl << "Lastly, tell me the id of that person" << std::endl;
                Student student(first_name, last_name, id);
                all_students.push_back(student);
            }
            if(second_input == 2)
            {
                std::cout << "Please tell me the ID you want me to look for" << std::endl;
                std::cin >> id;
                std::cout << std::endl << find_id(id, all_students);
            }
            if(second_input == 3)
            {
                
            }
            if(second_input == 4)
            {
                std::string serialized = hps::to_string(all_students);
                auto parsed = hps::from_string<std::vector<Course>>(serialized);

                std::ofstream dataFile;
                dataFile.open("student.txt", std::ios::binary | std::ios::app);
                dataFile << serialized << std::endl;
                dataFile.close();
            }
            if(second_input == 5)
            {
                
                
            }
        }
        
        if(input ==  'T')
        {
            std::cout << "Ahh, I see that you are interested in teachers. Would you like to construct a new course, or add students to courses?" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'1\'" << std::right << std::setfill('.') << std::setw(20) << " To construct a new course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'2\'" << std::right << std::setfill('.') << std::setw(20) << " To add students to a course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'3\'" << std::right << std::setfill('.') << std::setw(20) << " To add teachers to the course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'4\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to serialize the course information" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'5\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to display the information of the serialized file" << std::endl;
            std::cin >> second_input;
            if(second_input == 1)
            {
                std::cout << "Please, tell me the first name of the teacher" << std::endl;
                std::cin >> first_name;
                std::cout << std::endl << "Now, tell me the last name of the teacher" << std::endl;
                std::cin >> last_name;
                std::cout << std::endl << "Next, which department does this teacher belong to?" << std::endl;
                std::cin >> department;
                std::cout << std::endl << "Lastly, tell me the id of that teacher" << std::endl;
                Teacher teacher(first_name, last_name, department, id);
                all_teachers.push_back(teacher);
            }
            if(second_input == 2)
            {
                
            }
            if(second_input == 3)
            {
                
            }
            if(second_input == 4)
            {
                
            }
            if(second_input == 5)
            {
                
            }
        }

        if(input == 'L')
        {
            
        }

        if(input == 'Q')
        {
            std::cout << "Goodbye, and take care!!" << std::endl;
            break;
        }


    }
    return 0;
}